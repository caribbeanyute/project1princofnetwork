import sys
import getopt
import Checksum
import BasicSender
import os
import random
import Queue
#from timer import Timer
'''
This is a skeleton sender class. Create a fantastic transport protocol here.
'''
class Sender(BasicSender.BasicSender):
    def __init__(self, dest, port, filename, debug=False, sackMode=False):
        super(Sender, self).__init__(dest, port, filename, debug)
        self.sackMode = sackMode
        self.debug = debug
        self.message_size = 1471
        self.windowsize = 7
        self.maxwind = self.windowsize - 1
        self.window = Queue.LifoQueue(self.windowsize)
        #self.buffer = 
    # remove me before uploading
    def log(self, label, msg):
        """Logs some information to the screen if debugging is allowed.
        
        Arguments:
            label: A string labelling the data to be output
            msg: A packet containing data to be output

        Returns:
            None
        """
        if debug:
            if msg is None:
                print("Sender.py: %s" % label)
            else:
                if self.sackMode:
                    msg_type, sack_data, data, checksum = self.split_packet(msg)

                    print("Sender.py: %s %s|%s|%s|%s" %
                    (label, msg_type, sack_data, data[:5], checksum))
                else:
                    msg_type, seqno, data, checksum = self.split_packet(msg)
                    print("Sender.py: %s %s|%d|%s|%s" %
                    (label, msg_type, int(seqno), data[:5], checksum))

    def file_string(self,seq):
            data=[]
            l=[self.make_packet("syn",seq,"")]
            i=0
            while True:
                piece = self.infile.read(self.message_size)
                if piece == "":
                    break # end of file
                else:
                    data.append(piece)
                i+=1
            data_len = len(data)-1
            for index, x in enumerate(data):
                if index == data_len:
                    l.append(self.make_packet('fin',seq+index+1,x))
                else:
                    l.append(self.make_packet('dat',seq+index+1,x))
            return l

    
        
    # Main sending loop.
    def start(self):
        def chunker_list(seq, size):
            return (seq[i::size] for i in range(size))

        
        #seqno = random.randint(30,900)
        seqno = 0
        packets = self.file_string(seqno)
        ack = 0 # unaaked
        uack = 0 # oldestunacked
        while(uack < len(packets)):
            if uack < self.windowsize and (ack + uack) <len(packets):
               print(uack+ack)
               
               self.send(packets[uack+ack])
               ack += 1
               continue
            else:
                read_send = select.select([self.sock],[],[],self.timeout)
                if read_send[0]:
                   rcv =  self.receive()
                else:
                    ack = 0
                    continue
                if self.split_packet(rcv)[1] == uack:
                    uack +=1
                    ack-=1
                else:
                    ack = 0
                    continue
             

        
'''
This will be run if you run this script from the command line. You should not
change any of this; the grader may rely on the behavior here to test your
submission.
'''
if __name__ == "__main__":
    def usage():
        print "BEARS-TP Sender"
        print "-f FILE | --file=FILE The file to transfer; if empty reads from STDIN"
        print "-p PORT | --port=PORT The destination port, defaults to 33122"
        print "-a ADDRESS | --address=ADDRESS The receiver address or hostname, defaults to localhost"
        print "-d | --debug Print debug messages"
        print "-h | --help Print this usage message"
        print "-k | --sack Enable selective acknowledgement mode"

    try:
        opts, args = getopt.getopt(sys.argv[1:],
                               "f:p:a:dk", ["file=", "port=", "address=", "debug=", "sack="])
    except:
        usage()
        exit()

    port = 33122
    dest = "localhost"
    filename = None
    debug = False
    sackMode = False

    for o,a in opts:
        if o in ("-f", "--file="):
            filename = a
        elif o in ("-p", "--port="):
            port = int(a)
        elif o in ("-a", "--address="):
            dest = a
        elif o in ("-d", "--debug="):
            debug = True
        elif o in ("-k", "--sack="):
            sackMode = True

    s = Sender(dest,port,filename,debug, sackMode)
    try:
        s.start()
    except (KeyboardInterrupt, SystemExit):
        exit()
